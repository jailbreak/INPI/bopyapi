# bopyapi

Un serveur d'API qui rend transparent l'accès aux données BOPI et permet de lancer le processus de génération PDF via le script BOPY.

bopyapi repose sur [FastAPI](https://fastapi.tiangolo.com)

## Développement

### Dépendances

Utiliser un *virtualenv* de préférence.

```bash
pip install -r requirements.txt
pip install -e .
```

### Configuration

Copier le fichier `.env.example` vers `.env` et adapter les valeurs.

À propos de la variable `BOPI_BASE_DIR`, lire la section suivante sur les données d'exemple.

### Données d'exemple

Cf https://framagit.org/jailbreak/INPI/bopi-sample-data

```bash
git clone git@framagit.org:jailbreak/INPI/bopi-sample-data.git
```

Vérifiez dans votre fichier `.env` que la variable `BOPI_BASE_DIR` pointe bien vers le répertoire `./bopi-sample-data`.

### Services

L'application bopyapi repose sur le service celery qui lui-même est configuré pour utiliser redis comme *backend* et rabbitmq comme *broker*.

Pour démarrer les services, d'abord créer un répertoire `redis_data` puis :
```bash
docker-compose -f docker-compose.dev.yml up
```

Puis dans un autre terminal, en ayant le *virtualenv* chargé :
```bash
pip install watchdog  # si pas déjà fait
./worker.sh
```

Le script `worker.sh` recharge automatiquement le *worker* lorsqu'un fichier Python change (cf [cet article de blog](https://www.distributedpython.com/2019/04/23/celery-reload/)).

### Application

```bash
./serve.sh
```

Accès à l'API à l'adresse http://localhost:8000

### Tableau de bord des tâches

Cf https://flower.readthedocs.io/en/latest/install.html

En ayant le *virtualenv* chargé:
```bash
pip install flower  # si pas déjà fait
./tasks-dashboard.sh
```

Puis ouvrir http://localhost:5555/