from datetime import datetime, timezone

import ujson as json
from celery.result import AsyncResult
from celery.states import PENDING, STARTED

from . import clients


class Backend():
    """For each Bopi store:
    task_info_list_{bopi_id}: task_info list (only first one can be active)
    task_info is a json dict containing id and date_start
    (needed because result backend doesn't store date_start info, only date_done),
     e.g. :
    {
        "id": "9f6f1ddb-e7ba-43a3-b782-05fde0a08939",
        "date_start": "2019-11-05T15:54:38.622274",
    }
    """
    KEY_TPL = 'task_info_list:{}'

    def __init__(self):
        self.redis = clients.redis
        self.celery_app = clients.celery

    def get_task_info_list_and_count(self, bopi_id, limit):
        """Retrieve task dict list from redis (and celery).

        Return a tuple of tasks (max number set by `limit`), and the total count."""
        key = Backend.KEY_TPL.format(bopi_id)
        count = self.redis.llen(key)
        if count == 0:
            return ([], count)
        task_info_list = []
        for json_redis_task_info in self.redis.lrange(key, 0, limit):
            redis_task_info = json.loads(json_redis_task_info)
            task_info_list.append(self._compute_full_task_info(redis_task_info))
        return (task_info_list, count)

    def _compute_full_task_info(self, redis_task_info):
        """Factoring code"""
        task_id = redis_task_info.get('id')
        celery_task = AsyncResult(task_id, app=self.celery_app)
        return {
            **redis_task_info,
            "state": celery_task.state,
            # date_done is already a date as string without time zone info, but it's UTC
            "date_done": celery_task.date_done + '+00:00' if celery_task.date_done else None,
        }

    def get_running_task_info(self, bopi_id):
        """Retrieve task dict from redis (and celery) if there's a running task
        else return None"""
        key = Backend.KEY_TPL.format(bopi_id)
        if self.redis.llen(key) == 0:
            return None
        redis_task_info = json.loads(self.redis.lrange(key, 0, 1)[0])
        task_info = self._compute_full_task_info(redis_task_info)
        task_state = task_info.get('state')
        return task_info if task_state in (PENDING, STARTED) else None

    def register_task(self, bopi_id, task_id):
        """Register task info in redis"""
        date_start = datetime.now(timezone.utc).isoformat()
        key = Backend.KEY_TPL.format(bopi_id)
        redis_task_info = {
            'id': task_id,
            'date_start': date_start
        }
        self.redis.lpush(key, json.dumps(redis_task_info))

        self.redis.save()
        return self._compute_full_task_info(redis_task_info)
