from celery import Celery
from redis import Redis

from . import config

redis = Redis(host=config.REDIS_HOST, port=config.REDIS_PORT)

celery = Celery('bopyapi', backend=config.CELERY_BACKEND_URL, broker=config.CELERY_BROKER_URL,
                include=['bopyapi.tasks'])

# Cf # https://docs.celeryproject.org/en/latest/userguide/configuration.html
celery.conf.update(
    # Never expire task results in backend because we need to read them long after the task is complete,
    # to return it as a response of endpoints.
    result_expires=None,

    # Distinguish PENDING from STARTED.
    task_track_started=True,
)
