import re
from operator import itemgetter
from pathlib import Path
from typing import List
from urllib.parse import urljoin

from fastapi import FastAPI, HTTPException, Query
from pydantic import BaseModel
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import FileResponse

from . import bopi_common as bc
from . import config, task_backend, tasks

app = FastAPI(
    title='bopyapi',
    description='Remote control for bopi pdf generation',
    version=config.API_VERSION,
    openapi_prefix=config.API_BASE_PATH,
)

# CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


# Handy functions to compose JSON response
def common_response(endpoint, args={}):
    return {
        "_meta": {
            "api_version": config.API_VERSION,
            "endpoint": endpoint,
            "args": args,
        }
    }


def ok_response(endpoint, result_content, args={}):
    return {
        **common_response(endpoint, args),
        "success": True,
        "result": result_content,
    }


def err_response(endpoint, error_content, args={}):
    return {
        **common_response(endpoint, args),
        "success": False,
        "error": error_content,
    }

# home page
@app.get("/", include_in_schema=False)
def index():
    docs_url_path = urljoin(config.API_BASE_PATH, app.docs_url)
    return ok_response('/', {
        "message": "This is the home page of bopyapi. Its documentation is here: {}".format(docs_url_path)
    })

# Get bopi list
@app.get('/bopi')
def bopi_list(include_archived: bool = False):
    bopi_dirpath = config.BOPI_BASE_DIR
    bopi_info_list = [bc.get_bopi_info(bopi_id, bopi_dir)
                      for (bopi_id, bopi_dir) in bc.iter_bopi(bopi_dirpath, include_archived)]

    return ok_response("/bopi", {
        "numFound": len(bopi_info_list),
        "start": 0,
        "items": sorted(bopi_info_list, key=itemgetter('id'))
    })


# Common constraint on bopi_id
BOPI_ID_QUERY = Query(..., min_length=7, regexp=bc.BOPI_ID_PATTERN)

# Get detailed bopi information
@app.get('/bopi/{bopi_id}')
def bopi_show(bopi_id: str = BOPI_ID_QUERY):
    """Retrieve information about one BOPI"""

    bopi_dir = config.BOPI_BASE_DIR / bopi_id
    bopi_info = bc.get_bopi_info(bopi_id, bopi_dir, with_detail=True)

    if bopi_info is None:
        return err_response('/bopi/{bopi_id}', {'code': "BOPI_NOT_FOUND"},
                            {"bopi_id": bopi_id})

    task_info_list, count = get_task_info_list_and_count(bopi_id, limit=1)
    bopi_info = {
        **bopi_info,
        "tasks": {
            "latest": task_info_list[0] if count > 0 else None,
            "numFound": count,
        }
    }
    return ok_response("/bopi/{bopi_id}", bopi_info, {"bopi_id": bopi_id})

# Get access to PDF
@app.get('/bopi/{bopi_id}/pdf')
def bopi_pdf(bopi_id: str = BOPI_ID_QUERY):
    """Retrieve PDF for the given BOPI"""

    bopi_dir = config.BOPI_BASE_DIR / bopi_id
    if not bopi_dir.exists():
        return err_response('/bopi/{bopi_id}/pdf', {'code': "BOPI_NOT_FOUND"},
                            {"bopi_id": bopi_id})

    pdf_filepath = bc.get_pdf_output_filepath(bopi_dir)
    if pdf_filepath is None:
        return err_response('/bopi/{bopi_id}.pdf', {'code': "BOPI_PDF_NOT_FOUND"},
                            {"bopi_id": bopi_id})

    return FileResponse(str(pdf_filepath),
                        200,
                        media_type="application/pdf",
                        filename=pdf_filepath.name)


def get_task_info_list_and_count(bopi_id, limit):
    backend = task_backend.Backend()
    return backend.get_task_info_list_and_count(bopi_id, limit)

# Return bopi tasks
@app.get("/bopi/{bopi_id}/tasks")
def bopi_tasks(bopi_id: str = BOPI_ID_QUERY):
    bopi_dir = config.BOPI_BASE_DIR / bopi_id
    bopi_info = bc.get_bopi_info(bopi_id, bopi_dir)
    if bopi_info is None:
        return err_response('/bopi/{bopi_id}/tasks', {'code': 'BOPI_NOT_FOUND'}, {"bopi_id": bopi_id})
    if bopi_info.get('archived') == True:
        return err_response('/bopi/{bopi_id}/tasks', {'code': 'BOPI_ARCHIVED'}, {"bopi_id": bopi_id})

    task_info_list, count = get_task_info_list_and_count(bopi_id, config.TASKS_LIMIT)
    return ok_response("/bopi/{bopi_id}/tasks", {
        "numFound": count,
        "start": 0,
        "items": task_info_list
    }, {'bopi_id': bopi_id})


def compute_bopy_publish_command(bopi_id, bopi_dir: Path, with_color, section_list):
    """return command token list ready to be executed by a subprocess.run"""
    return [
        config.BOPY_SCRIPT_PATH,
        "-v",
        "publish",
        "--sections",
        ','.join(section_list),
        "--color" if with_color else "--no-color",
        "--scribus-path",
        config.SCRIBUS_CMD,
        "--bopi-dir",
        str(config.BOPI_BASE_DIR),
        bopi_id,
    ]


class PublishTaskParameters(BaseModel):
    color: bool
    sections: List[str]


@app.post("/bopi/{bopi_id}/run")
def bopi_run(taskParams: PublishTaskParameters, bopi_id: str = BOPI_ID_QUERY):
    """Run BOPY publish task"""
    bopi_dir = config.BOPI_BASE_DIR / bopi_id
    bopi_info = bc.get_bopi_info(bopi_id, bopi_dir)
    if bopi_info is None:
        return err_response('/bopi/{bopi_id}/run', {'code': 'BOPI_NOT_FOUND'}, {"bopi_id": bopi_id})
    if bopi_info.get('archived') == True:
        return err_response('/bopi/{bopi_id}/run', {'code': 'BOPI_ARCHIVED'}, {"bopi_id": bopi_id})

    backend = task_backend.Backend()

    # Is a task already running?
    task_info = backend.get_running_task_info(bopi_id)
    if task_info is not None:
        return err_response("/bopi/{bopi_id}/run", {'code': 'BOPI_ALREADY_RUNNING', 'task': task_info}, {'bopi_id': bopi_id})

    # Nope. Cool! let's run a new one
    bopi_cmd = compute_bopy_publish_command(bopi_id, bopi_dir, taskParams.color, taskParams.sections)
    celery_task = tasks.run_bopy.delay(bopi_id, bopi_cmd)

    # And register it in redis
    task_info = backend.register_task(bopi_id, celery_task.id)
    return ok_response("/bopi/{bopi_id}/run", {'task': task_info}, {'bopi_id': bopi_id})
