import logging
import os
import sys
from distutils.util import strtobool
from pathlib import Path

import pkg_resources
from dotenv import load_dotenv

# Logger
log = logging.getLogger(__name__)

# Load settings from .env file if exists
load_dotenv()

# Logger config
LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO").upper()
logging.basicConfig(level=LOG_LEVEL)

# API config
API_BASE_PATH = os.environ.get("API_BASE_PATH") or "/"

try:
    API_VERSION = pkg_resources.get_distribution("bopyapi").version
except pkg_resources.DistributionNotFound:
    API_VERSION = "development"

TASKS_LIMIT = int(os.environ.get("TASKS_LIMIT") or 100)

# Where to find BOPI folders
BOPI_BASE_DIR = os.environ.get("BOPI_BASE_DIR") or None
if BOPI_BASE_DIR is None:
    log.error("BOPI_BASE_DIR environment variable is not set, stopping here.")
    sys.exit(1)
BOPI_BASE_DIR = Path(BOPI_BASE_DIR)
if not BOPI_BASE_DIR.exists():
    log.error("Can't find BOPI_BASE_DIR: [%s], stopping here.", BOPI_BASE_DIR)
    sys.exit(1)

# SCRIBUS PATH
SCRIBUS_CMD = os.environ.get("SCRIBUS_CMD") or "scribus"

# BOPY script
BOPY_SCRIPT_PATH = os.environ.get("BOPY_SCRIPT_PATH") or "bopy"

# Celery config
CELERY_BACKEND_URL = os.environ.get("CELERY_BACKEND_URL") or None
if CELERY_BACKEND_URL is None:
    log.error("CELERY_BACKEND_URL environment variable is not set, stopping here.")
    sys.exit(1)
CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL") or None
if CELERY_BROKER_URL is None:
    log.error("CELERY_BROKER_URL environment variable is not set, stopping here.")
    sys.exit(1)

# Redis config
REDIS_HOST = os.environ.get("REDIS_HOST") or "localhost"
REDIS_PORT = int(os.environ.get("REDIS_PORT")) if os.environ.get("REDIS_PORT") else 6379
