import os
import subprocess
import sys
from typing import List

from . import bopi_common, clients, config

app = clients.celery


class BopyError(Exception):
    def __init__(self, message, return_code=None, output=None):
        self.message = message
        self.return_code = return_code
        self.output = output

        # Cf https://docs.celeryproject.org/en/latest/userguide/tasks.html#creating-pickleable-exceptions
        super(BopyError, self).__init__(message, return_code, output)


@app.task()
def run_bopy(bopi_id: str, bopi_cmd: List[str]):
    try:
        output = subprocess.check_output(bopi_cmd, stderr=subprocess.STDOUT, universal_newlines=True)
    except subprocess.CalledProcessError as exc:
        raise BopyError("Failed calling subprocess", return_code=exc.returncode, output=exc.output)

    # Check that PDF exists
    bopi_dir = config.BOPI_BASE_DIR / bopi_id
    if not bopi_dir.exists():
        raise BopyError("Le répertoire du BOPI [{}] n'existe pas".format(str(bopi_dir)))

    pdf_output_filepath = bopi_common.get_pdf_output_filepath(bopi_dir)
    if not pdf_output_filepath:
        raise BopyError("Aucun fichier PDF n'a été généré")

    return output
