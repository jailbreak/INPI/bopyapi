import re
from datetime import datetime
from operator import itemgetter
from pathlib import Path

import yaml

BOPI_ID_PATTERN = r'^\d{4}-\d{1,2}$'
BOPI_ID_REGEX = re.compile(BOPI_ID_PATTERN)


def compute_file_info(f: Path):
    """Create information dictionary from existing file path"""
    return {
        "name": f.name,
        "updated_at": datetime.fromtimestamp(f.stat().st_mtime).astimezone().isoformat(),
        "size": f.stat().st_size,
    }


def get_pdf_output_filepath(bopi_dir: Path):
    """Look for PDF output file in bopi dir, return None if not found"""
    pdf_output_dir = bopi_dir / 'output' / 'pdf'
    if not pdf_output_dir.exists():
        return None
    pdf_files = sorted(pdf_output_dir.glob('*.pdf'))
    if pdf_files:
        return pdf_files[0]
    return None


def is_bopi_archived(bopi_dir: Path):
    """Return true if a bopi is archived.
    Based on "archive" subdir existence"""
    archive_dir = bopi_dir / 'archive'
    return archive_dir.exists and archive_dir.is_dir()


def iter_bopi(bopi_base_dir: Path, include_archived=False):
    """Yield (bopi_id, bopi_dir) tuples"""
    for bopi_dir in bopi_base_dir.glob('*'):
        if not bopi_dir.is_dir() or not BOPI_ID_REGEX.match(bopi_dir.name):
            continue
        if is_bopi_archived(bopi_dir) and not include_archived:
            continue
        yield (bopi_dir.name, bopi_dir)


def get_bopi_info(bopi_id: str, bopi_dir: Path, with_detail=False):
    """Return detail of bopi from bopi dir"""

    if not bopi_dir.exists():
        return None

    # Minimum information
    info = {
        'id': bopi_id
    }

    # Is bopi archived?
    info['archived'] = is_bopi_archived(bopi_dir)

    # Get information from YAML configuration file
    yaml_dir = bopi_dir / 'output' / 'yaml'
    if yaml_dir.exists():
        yaml_files = list(yaml_dir.glob('*.yaml'))
        if yaml_files:
            # Select the first one
            with yaml_files[0].open('r', encoding='utf-8') as fd:
                yaml_conf = yaml.safe_load(fd)
                if yaml_conf.get('date_publication'):
                    info['published_at'] = yaml_conf['date_publication']
                if yaml_conf.get('numeros_publication'):
                    info['published_range'] = yaml_conf['numeros_publication']

    # More information (only if wanted)
    if with_detail:

        # Input files
        input_dir = bopi_dir / 'input'
        input_files = []
        if input_dir.exists():
            input_files = [compute_file_info(f) for f in input_dir.glob('*') if f.is_file()]
        info['input_files'] = input_files

        # Generated pdf
        pdf_filepath = get_pdf_output_filepath(bopi_dir)
        if pdf_filepath:
            info['pdf'] = {**compute_file_info(pdf_filepath), "path": "/bopi/{}/pdf".format(bopi_id)}

    return info
