## 0.1.0

- bopyapi is defined as a package
- /bopi endpoint : list available bopis
- /bopi/{bopiId} : get bopi detail
- /bopi/{bopiId}/pdf : get generated PDF file associated to bopi
- /bopi/{bopiId}/tasks : get publish tasks for this bopi
- /bopi/{bopiId}/run (POST) : run publish task for this bopi
