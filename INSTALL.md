# Instructions d'installation sur un serveur CENTOS

L'application bopyweb permet la visualisation de bopi et le lancement du script bopy.
Elle doit être installée sur un serveur contenant déjà une version de bopy fonctionnelle
(cf. [guide d'administration de bopy](https://pad.lamyne.org/bopy-README.md#tip1))

Ce déroulé d'installation indique étape par étape les opérations à réaliser pour installer l'application `bopyweb` sur un serveur (nommé ici `dev-inpi.anybox.cloud`)

À la fin de l'installation, les urls suivantes sont fonctionnelles :

- /bopyweb : application bopyweb
- /bopyapi : API d'interrogation des bopi et de lancement de bopy
- /bopyapi/docs : documentation de l'API
- /dashboard : Tableau de bord (celery flower) des tâches de publication en cours

Ce guide d'installation se base donc sur l'installation préalable de l'environnement Python (nécessaire à bopy)

Pour vérifier la présence et la version de Python :

```bash
> python3.6 --version
Python 3.6.8
```

L'outil git doit également être installé, pour vérifier la présence et la version de git :

```bash
> git --version
git version 1.8.3.1
```

L'installation de bopyweb s'effectue

- dans les dossiers :
  - /home/inpi/.bopyweb
  - /home/inpi/.bopyapi
- avec les contraintes suivantes :
  - pas d'utilisation de virtualenv python
  - pas d'utilisation de docker
- avec les briques logicielles déjà installées sur le serveur :
  - bopy dans /home/inpi/.inpi et accessible via le script /usr/local/bin/bopy
  - scribus dans /home/inpi/.scribus

Il est entendu que les bopi « Dessins et Modèles » sont situés dans le dossier `/home/inpi/Dessins et Modèles`

## Instructions

### Installer les dépendances logicielles

#### Environnement Python

```bash
sudo yum install python3-devel.x86_64 libuv
```

Paquets supplémentaires à installer lorsque l'on n'utilise pas les environnements virtuels Python :

```bash
sudo pip3 install setuptools pip wheel
```

##### Note

```
Attention, l'installation du module `uvtools` (dépendance du module `uvicorn` utilisé par le projet bopyapi)
nécessite de générer et compiler du code dans /tmp

Cette installation échoue si la partition /tmp est configurée avec l'option "noexec". Cette option doit être
désactivée lors de l'installation et peut être réactivée ensuite.
```

#### NodeJs

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install node
```

#### rabbitmq-server

```bash
sudo yum install rabbitmq-server
sudo systemctl start rabbitmq-server
sudo systemctl enable rabbitmq-server
```

#### redis

```bash
sudo yum install redis
sudo systemctl start redis
sudo systemctl enable redis
```

### Récupérer le code des applications et les initialiser

_À faire en dehors de tmux pour éviter les problèmes de SSH_

```bash
git clone git@framagit.org:jailbreak/INPI/bopyapi.git ~/.bopyapi
cd ~/.bopyapi
sudo pip3 install -r requirements.txt
sudo pip3 install -e .

git clone git@framagit.org:jailbreak/INPI/bopyweb.git ~/.bopyweb
cd ~/.bopyweb
npm install
npm run build
```

#### Configurer les instances

**bopyapi**

Fichier de configuration /home/inpi/.bopyapi/.env (à créer s'il n'existe pas):

```
BOPI_BASE_DIR=/home/inpi/Dessins et modèles

API_BASE_PATH=/bopyapi

SCRIBUS_CMD=/home/inpi/.scribus/scribus

BOPY_SCRIPT_PATH=/usr/local/bin/bopy

CELERY_BACKEND_URL=redis://localhost
CELERY_BROKER_URL=pyamqp://

REDIS_HOST=localhost
REDIS_PORT=6379
```

**bopyweb**

Fichier de configuration /home/inpi/.bopyweb/.env (à créer s'il n'existe pas):

```
# Section codes
SECTION_CODES=APUB|APRO|COUV1|SOM|PROC|LOC|ST80|ST3|OMPI|DDG|ISOM|I11|I12|I2SOM|I21|I22|I3SOM|I31|I32|IISOM|II1|II2|II3|III|DP|COUV4

# Section code meaning "All sections"
ALL_SECTIONS_CODE=ALL

API_URL=http://dev-inpi.anybox.cloud/bopyapi

BASE_PATH=/bopyweb

FLOWER_URL=http://dev-inpi.anybox.cloud/dashboard
```

**Accès web (configuration Apache)**

Dans le fichier `/etc/httpd/conf.d/dev-inpi.anybox.cloud.conf`, ajouter la configuration suivante

```conf
# BOPYAPI
ProxyPass "/bopyapi" "http://localhost:8000"
ProxyPassReverse "/bopyapi" "http://localhost:8000"

# BOPYWEB
ProxyPass "/bopyweb" "http://localhost:3000/bopyweb"

# Flower celery dashboard
<Location /dashboard>
    ProxyPass "http://localhost:5555"
    RewriteEngine On
    RewriteRule "/dashboard/(.*)" "/$1"
</Location>
```

Relancer le serveur web (Apache) :

```bash
sudo systemctl restart httpd
```

#### Configurer les services systemd

**bopyweb**

Créer le script `systemd` /etc/systemd/system/bopyweb.service avec le contenu suivant :

```
[Unit]
Description=BOPYWEB UI
After=network.target

[Service]
User=inpi
WorkingDirectory=/home/inpi/.bopyweb
ExecStart=/home/inpi/.nvm/versions/node/v13.0.1/bin/node --require dotenv/config __sapper__/build

[Install]
WantedBy=multi-user.target
```

```
cd /etc/systemd/user
sudo ln -s ../system/bopyweb.service
```

**bopyapi**

Créer le script `systemd` /etc/systemd/system/bopyapi.service avec le contenu suivant :

```
[Unit]
Description=BOPYAPI
After=network.target

[Service]
User=inpi
WorkingDirectory=/home/inpi/.bopyapi
ExecStart=/usr/local/bin/uvicorn bopyapi:app

[Install]
WantedBy=multi-user.target
```

```
cd /etc/systemd/user
sudo ln -s ../system/bopyapi.service
```

**celery**

Créer le script `systemd` /etc/systemd/system/celery.service avec le contenu suivant :

```
[Unit]
Description=Celery service for BOPYAPI
After=network.target

[Service]
User=inpi
WorkingDirectory=/home/inpi/.bopyapi
ExecStart=/usr/local/bin/celery worker -A bopyapi.clients:celery --loglevel info

[Install]
WantedBy = multi-user.target
```

**flower**

Créer le script `systemd` /etc/systemd/system/celery-flower.service avec le contenu suivant :

```
[Unit]
Description=Celery dashboard service
After=network.target

[Service]
User=inpi
WorkingDirectory=/home/inpi/.bopyapi
ExecStart=/usr/local/bin/flower -A bopyapi.clients:celery --port=5555 --url_prefix=dashboard

[Install]
WantedBy=multi-user.target
```

```
cd /etc/systemd/user
sudo ln -s ../system/celery-flower.service
```

#### Lancer les services

```bash
sudo systemctl start bopyweb
sudo systemctl start bopyapi
sudo systemctl start celery
sudo systemctl start celery-flower
```
